package utils;

public enum Criteria {
    DEGREE("Degree", "Degree"),
    CREDITS("Credits", "Credits");

    private String description;
    private String code;

    Criteria(String description, String code) {
        this.description = description;
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public String getCode() {
        return code;
    }
}
