package validations;


import utils.Criteria;

import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Validator implements IValidator {

    private final static Logger LOGGER =
            Logger.getLogger(Validator.class.getName());

    @Override
    public void validate(Map<String, Object> parameters) {
        LOGGER.log(Level.INFO, "Applying validations");
        parameters.keySet().forEach(k -> {
            Object value = parameters.get(k);
            if (null == parameters.get(k)) {
                throw new IllegalArgumentException(String.format("%s cannot be null", k));
            }
        });
    }

    @Override
    public Object extractValue(Map<String, Object> parameters, Criteria property) {
        Object value = parameters.get(property.getCode());

        if (value == null) {
            throw new IllegalArgumentException(String.format("%s cannot be null", property.getCode()));
        }
        return value;
    }

}
