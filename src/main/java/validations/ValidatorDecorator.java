package validations;

import utils.Criteria;

import java.util.Map;

public abstract class ValidatorDecorator implements IValidator {

    private IValidator validator;

    public ValidatorDecorator(IValidator validator) {
        this.validator = validator;
    }

    @Override
    public void validate(Map<String, Object> parameters) {
        validator.validate(parameters);
    }

    @Override
    public Object extractValue(Map<String, Object> parameters, Criteria property) {
        return validator.extractValue(parameters, property);
    }
}
