package validations;

import utils.Criteria;

import java.util.List;
import java.util.Map;

public class DegreesValidator extends ValidatorDecorator {

    private List<String> validDegrees;

    public DegreesValidator(List<String> validDegrees, IValidator validator) {
        super(validator);
        this.validDegrees = validDegrees;
    }

    public void validate(Map<String, Object> parameters) {
        super.validate(parameters);
        String currentDegree = (String) extractValue(parameters, Criteria.DEGREE);

        if (!validDegrees.contains(currentDegree)) {

            throw new NullPointerException("Degree not found");
        }

    }

}
