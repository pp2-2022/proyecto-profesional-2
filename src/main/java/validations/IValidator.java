package validations;

import utils.Criteria;

import java.util.Map;

public interface IValidator {

    void validate(Map<String,Object> parameters);

    Object extractValue(Map<String,Object> parameters, Criteria property);

}
