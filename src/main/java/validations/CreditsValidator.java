package validations;

import utils.Criteria;

import java.util.Map;

public class CreditsValidator extends ValidatorDecorator {

    private Integer capOfCredits;

    public CreditsValidator(Integer capOfCredits, IValidator validator) {
        super(validator);
        this.capOfCredits = capOfCredits;
    }


    public void validate(Map<String, Object> parameters) {
        super.validate(parameters);
        Integer currentCredit =(Integer) extractValue(parameters, Criteria.CREDITS);

        if (currentCredit > capOfCredits) {
            throw new NullPointerException(String.format("No events found for %s credits", currentCredit));
        }

    }
}
