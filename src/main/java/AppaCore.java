import core.InformationRetriever;
import core.PluginDiscovery;
import validations.CreditsValidator;
import validations.DegreesValidator;
import validations.IValidator;
import validations.Validator;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

public class AppaCore {

    private InformationRetriever informationRetriever;

    private final List<String> currentDegrees = Arrays.asList("Licenciatura en sistemas", "Licenciatura en Comunicacion", "Licenciatura en Quimica",
            "Ingenieria Industrial", "Tecnicatura Superior en Informatica");


    public void init() {//fixme consultar si importar desde un json o un csv el listado de materias podria ser una story
        IValidator validator = new DegreesValidator(currentDegrees, new CreditsValidator(4, new Validator()));
        informationRetriever = new InformationRetriever(validator);
    }

    public String doSearch(String degree, String credits) {

        return this.informationRetriever.validateAndSearch(degree, credits);
    }

    public static void main (String [] args) throws ClassNotFoundException, InvocationTargetException, InstantiationException, IllegalAccessException, NoSuchMethodException, URISyntaxException, IOException {

        PluginDiscovery pluginDiscovery = new PluginDiscovery();
        pluginDiscovery.generatePlugIn("plugs"); //FIXME this shouldn't be a parameter
    }


}
