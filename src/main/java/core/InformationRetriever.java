package core;

import utils.Criteria;
import validations.IValidator;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class InformationRetriever {

    private final static Logger LOGGER =
            Logger.getLogger(InformationRetriever.class.getName());

    private IValidator validator;

    public InformationRetriever(IValidator validator) {
        this.validator = validator;
    }


    public String validateAndSearch(String degree, String credits) {

        Map<String, Object> searchCriteria = new HashMap<>();
        searchCriteria.put(Criteria.DEGREE.getCode(), degree);
        searchCriteria.put(Criteria.CREDITS.getCode(),credits);

        LOGGER.log(Level.INFO, "Validating search");

        validator.validate(searchCriteria);

        return "values returned";
    }

}
