package core;

import Scraping.Scraper;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.HashSet;
import java.util.Set;

public class PluginDiscovery {
    //fixme add property file with the location of the plug in
    public Set<Object> generatePlugIn(String path) throws URISyntaxException, IOException {
        Set<Object> result = new HashSet<>();

        try {

            File file = new File(path);

            //convert the file to URL format
            URL url = file.toURI().toURL();
            URL[] urls = new URL[]{url};

            //load this folder into Class loader
            ClassLoader cl = new URLClassLoader(urls);

            for (File f : file.listFiles()) {
                if (!f.getName().endsWith(".class")) continue;

                String name = f.getName().substring(0, f.getName().indexOf("."));

                Class cls = cl.loadClass(name);
                if (!Scraper.class.isAssignableFrom(cls))
                    throw new RuntimeException("Founded class don't qualify");
                result.add(cls.getDeclaredConstructor().newInstance());
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return result;
    }

}
