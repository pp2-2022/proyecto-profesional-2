import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import utils.Criteria;
import validations.CreditsValidator;
import validations.DegreesValidator;
import validations.IValidator;
import validations.Validator;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ValidationsTest {

    private IValidator validator;

    @BeforeEach
    public void setUp() {

        List<String> degrees = Arrays.asList("Licenciatura en Sistemas", "Licenciatura en Comunicacion");
        validator = new DegreesValidator(degrees, new CreditsValidator(4, new Validator()));
    }

    @Test
    public void SearchWithNullCredits() {

        Map<String, Object> searchCriteria = generateCriteria("Licenciatura en Sistemas", null);

        Exception exception = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            validator.validate(searchCriteria);
        });

        Assertions.assertEquals("Credits cannot be null", exception.getMessage());
    }

    @Test
    public void SearchWithNullDegree() {

        Map<String, Object> searchCriteria = generateCriteria(null, 3);

        Exception exception = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            validator.validate(searchCriteria);
        });

        Assertions.assertEquals("Degree cannot be null", exception.getMessage());
    }

    @Test
    public void searchForHighCredtis() {

        Map<String, Object> searchCriteria = generateCriteria("Licenciatura en Sistemas", 5);

        Exception exception = Assertions.assertThrows(NullPointerException.class, () -> {
            validator.validate(searchCriteria);
        });

        Assertions.assertEquals("No events found for 5 credits", exception.getMessage());
    }

    @Test
    public void searchForInexistentDegree() {

        Map<String, Object> searchCriteria = generateCriteria("Licenciatura en Sonido e Imagen", 3);
        Exception exception = Assertions.assertThrows(NullPointerException.class, () -> {
            validator.validate(searchCriteria);
        });

        Assertions.assertEquals("Degree not found", exception.getMessage());
    }

    private Map<String, Object> generateCriteria(String degree, Integer credits) {

        Map<String, Object> searchCriteria = new HashMap<>();
        searchCriteria.put(Criteria.DEGREE.getCode(), degree);
        searchCriteria.put(Criteria.CREDITS.getCode(), credits);

        return searchCriteria;
    }
}
